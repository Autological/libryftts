/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */

#ifndef LIBRYFTTS_H
#define LIBRYFTTS_H

/* Platforms */
#define RYFTTS_WIN		1
#define RYFTTS_FREE		2
#define RYFTTS_LINUX	3
#define RYFTTS_CYGWIN	4

/* Compilers */
#define RYFTTS_VS		5
#define RYFTTS_CLANG	10
#define RYFTTS_GCC		15

/* WordSize */
#define RYFTTS_32		20
#define RYFTTS_64		40


/* Ryftts Platform */
#ifdef _WIN32
#	define RYFTTS_PLATFORM	RYFTTS_WIN
#elif defined( __CYGWIN__ )
#	define RYFTTS_PLATFORM	RYFTTS_CYGWIN
#elif defined( __FreeBSD__ )
#	define RYFTTS_PLATFORM	RYFTTS_FREE
#elif defined( __linux )
#	define RYFTTS_PLATFORM	RYFTTS_LINUX
#endif

/* Ryftts Compiler */
#ifdef _MSC_VER
#	define RYFTTS_COMPILER	RYFTTS_VS 
#elif defined( __MINGW__ ) || defined( __MINGW32__ )
#	define RYFTTS_COMPILER	RYFTTS_MINGW
#elif defined( __clang__ )
#	define RYFTTS_COMPILER	RYFTTS_CLANG
#elif defined( __GNUC__ )
#	define RYFTTS_COMPILER	RYFTTS_GCC
#	define RYFTTS_GCC_VER \
	( __GNUC__ * 10000 + __GUNC_MINOR__ * 100 + __GNUC_PATCHLEVEL__ )
#endif

/* Ryftts Word Size */
#if defined( __Win64 ) || __WORDSIZE == 64 || __x86_64__ || __ppc64__
#	define RYFTTS_WORDSIZE	RYFTTS_64
#else 
#	define RYFTTS_WORDSIZE	RYFTTS_32
#endif

#define RYFTTS_ENVIRONMENT \
	( RYFTTS_PLATFORM + RYFTTS_COMPILER + RYFTTS_WORDSIZE )


/* Ryftts Versioning levels */
#define RYFTTS_VER_MAJOR 0
#define RYFTTS_VER_MINOR 1
#define RYFTTS_VER_PATCH 0
#define RYFTTS_VERSION \
	( RYFTTS_VER_MAJOR * 10000 + RYFTTS_VER_MINOR * 100 + RYFTTS_VER_PATCH )

/* basic typedefs */
typedef unsigned char uint8;
typedef char int8;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned int uint32;
typedef int int32;

#if RYFTTS_WORDSIZE == RYFTTS_64
typedef unsigned long uint64;
typedef long int64;
#endif

// Boolean shiving
#ifndef __cplusplus
#ifndef bool
typedef uint8 bool;
#define false ((bool)0)
#define true ((bool)1)
#define FALSE false
#define TRUE true
#endif
#else
#define bool uint8
#endif

#if RYFTTS_PLATFORM == RYFTTS_WIN || RYFTTS_PLATFORM == RYFTTS_CYGWIN
#	define RYFTTS_DLL_IMPORT __declspec(dllimport)
#	define RYFTTS_DLL_EXPORT __declspec(dllexport)
#	define RYFTTS_DLL_LOCAL
#elif RYFTTS_PLATFORM == RYFTTS_LINUX || RYFTTS_PLATFORM == RYFTTS_FREE
#	define RYFTTS_DLL_IMPORT	__attribute__ ((visibility ("default")))
#	define RYFTTS_DLL_EXPORT	__attribute__ ((visibility ("default")))
#	define RYFTTS_DLL_LOCAL		__attribute__ ((visibility ("hidden")))
#else
#	define RYFTTS_DLL_IMPORT
#	define RYFTTS_DLL_EXPORT
#	define RYFTTS_DLL_LOCAL
#endif


#if defined( RYFTTS_SHARED_IMPORT )
#	define RYFTTS_API RYFTTS_DLL_IMPORT
#	define RYFTTS_LOCAL RYFTTS_DLL_LOCAL
#elif defined( RYFTTS_SHARED_EXPORT )
#	define RYFTTS_API RYFTTS_DLL_EXPORT
#	define RYFTTS_LOCAL RYFTTS_DLL_LOCAL
#else
#	define RYFTTS_API
#	define RYFTTS_LOCAL
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if RYFTTS_PLATFORM == RYFTTS_WIN
#include <Windows.h>
#include <libryftts_types_win.h>
#else
#include <ncurses.h>
#include <libryftts_types_nix.h>
#endif


#include <sys.h>
#include <color.h>
#include <console.h>
#include <fov.h>

/* Yet to be implemented

#include "path.h"

*/

#ifdef __cplusplus
#undef bool
}
#endif

#endif
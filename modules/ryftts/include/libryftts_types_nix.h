/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */


#ifndef LIBRYFTTSYPES_H
#define LIBRYFTTSYPES_H
/* Color Types */
typedef uint8 RYFTTS_color_pair;

/* Colors */
typedef enum {
	RYFTTS_COLOR_BLACK,
	RYFTTS_COLOR_RED,
	RYFTTS_COLOR_GREEN,
	RYFTTS_COLOR_YELLOW,
	RYFTTS_COLOR_BLUE,
	RYFTTS_COLOR_PURPLE,
	RYFTTS_COLOR_CYAN,
	RYFTTS_COLOR_WHITE
} RYFTTS_color;

/* Console Types */
typedef struct {
	uint8 c;
	RYFTTS_color_pair color;
} RYFTTS_console_color_cell;

typedef struct {
	WINDOW *win;
	uint32 w;
	uint32 h;
	struct {
		uint32 x;
		uint32 y;
	} cursor;
	RYFTTS_console_color_cell *buffer;
	bool has_color;
} *RYFTTS_console;

/* Map Types */
/* Fov Map */
typedef enum {
	FOV_CIRCLE_SPARSE,
	FOV_CIRCLE_EXACT,
	FOV_CIRCLEHICK,
	FOV_DIAMOND_SPARSE,
	FOV_DIAMOND_EXACT,
	FOV_DIAMONDHICK,
	FOV_ALGO_SIZE
} RYFTTS_fov_algorithm;

typedef struct {
	bool* blocksView;
	uint32 width;
	uint32 height;
} RYFTTS_fov_map_struct;
typedef RYFTTS_fov_map_struct* RYFTTS_fov_map;

/* Fov View Map */
typedef struct {
	bool* in_sight;
	uint32 width;
	uint32 height;
	uint32 radius;
	uint32 x; 
	uint32 y;
} RYFTTS_fov_view_map_struct;
typedef RYFTTS_fov_view_map_struct * RYFTTS_fov_view_map;

#endif

/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *			LibRyftts: A roguelike development library
 *
 *			Ryftts-Console: Console library for libRyftts
 *
 *  File	: io_nix.c
 *  Purpose	: *nix console I/O system
 *
 *	Notes	:
 *	Author	: Luke Smith
 *	Date	: December 29, 2015
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * INCLUDES 
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libryftts.h>

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function Definitions
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_putentryat
 *		Purpose: put a ascii char to the buffer directly
 *		Args   : RYFTTS_console con,
 *				 uint8 c,
 *				 RYFTTS_color_pair color
 * 				 int x,
 *				 int y,
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_putentryat(RYFTTS_console con, uint8 c,
	RYFTTS_color_pair color, uint8 x, uint8 y)
{
	con->buffer[y * con->w + x] = 
		(RYFTTS_console_color_cell){.c = c, .color = color};
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_putc
 *		Purpose: put a character to the buffer
 *				 and updates console position.
 *		Args   : RYFTTS_console con,
 *				 uint8 c,
 *				 RYFTTS_color_pair color
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_putc(RYFTTS_console con, uint8 c,
	RYFTTS_color_pair color)
{
	con->buffer[con->cursor.y * con->w + con->cursor.x] = 
		(RYFTTS_console_color_cell){.c = c, .color = color};

	if(++con->cursor.x > con->w)
	{
		con->cursor.x = 0;
		if(++con->cursor.y > con->h)
			con->cursor.y = 0;
	}
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_puts
 *		Purpose: put a character string to the screen
 *				 and updates console position.
 *		Args   : RYFTTS_console con,
 *				 const RYFTTS_console_color_cell *cstr,
 *				 uint16 len
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_puts(RYFTTS_console con, 
	const RYFTTS_console_color_cell* cstr,
	uint16 len)
{
	const RYFTTS_console_color_cell *pstr = cstr;
	for(uint16 i = 0; i < len; i++)
	{
		RYFTTS_console_putc(con, pstr->c, cstr->color);
		pstr++;
	}
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_getc
 *		Purpose: get a keyboard code from the current console
 *		Args   : RYFTTS_console con
 *		Returns: uint8
 * ---------------------------------------------------------------------------
 */
uint8 RYFTTS_console_getc(RYFTTS_console con)
{
	return wgetch(con->win);
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_flush
 *		Purpose: flush current console's buffer to screen
 *		Args   : RYFTTS_console con
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_flush(RYFTTS_console con)
{
	for(uint8 i = 0; i < con->w * con->h; i++)
	{
		if(con->has_color) attron(COLOR_PAIR(con->buffer[i].color));
		mvwaddch(con->win, i / con->w, i - (i / con->w) % con->w, (chtype)con->buffer[i].c);
		if(con->has_color) attroff(COLOR_PAIR(con->buffer[i].color));
	}
}
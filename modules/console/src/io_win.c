/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *			LibRyftts: A roguelike development library
 *
 *			Ryftts-Console: Console library for libRyftts
 *
 *  File	: io_win.c
 *  Purpose	: Windows console I/O system
 *
 *	Notes	:
 *	Author	: Luke Smith
 *	Date	: December 29, 2015
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * INCLUDES 
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libryftts.h>

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function Definitions
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_putentryat
 *		Purpose: put a ascii char to the buffer directly
 *		Args   : RYFTTS_console con,
 *				 uint8 c,
 *				 RYFTTS_color_pair color
 * 				 int x,
 *				 int y,
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_putentryat(RYFTTS_console con, uint8 c,
	RYFTTS_color_pair color, uint8 x, uint8 y)
{
	con->buffer[y * con->w + x] = 
		(RYFTTS_console_color_cell){.c = c, .color = color};
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_putc
 *		Purpose: put a character to the buffer
 *				 and updates console position.
 *		Args   : RYFTTS_console con,
 *				 uint8 c
 *				 RYFTTS_color_pair color
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_putc(RYFTTS_console con, uint8 c,
	RYFTTS_color_pair color)
{
	con->buffer[y * con->w + x] = 
		(RYFTTS_console_color_cell){.c = c, .color = color};
	
	if(++con->cursor.x > con->w)
	{
		con->cursor.x = 0;
		if(++con->cursor.y > con->h)
			con->cursor.y = 0;
	}
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_puts
 *		Purpose: put a character string to the screen
 *				 and updates console position.
 *		Args   : RYFTTS_console con,
 *				 const RYFTTS_console_color_cell *cstr,
 *				 uint16 len
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_puts(RYFTTS_console con,
	const RYFTTS_console_color_cell* cstr,
	uint16 len)
{
	const uint8 *pstr = cstr;
	while( *pstr++ )
		RYFTTS_console_putc(con, *pstr);
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_getc
 *		Purpose: get a keyboard code from the current console
 *		Args   : RYFTTS_console con
 *		Returns: uint8
 * ---------------------------------------------------------------------------
 */
uint8 RYFTTS_console_getc(RYFTTS_console con)
{
	DWORD err = 0;
	INPUT_RECORD* buf = malloc(sizeof(TCHAR));
	ReadConsoleInput(con->hstdin, buf, 1, &err);
	return buf->Event.KeyEvent.uChar.AsciiChar;
}

/*
 * ---------------------------------------------------------------------------
 *		Name   : RYFTTS_console_flush
 *		Purpose: flush current console's buffer to screen
 *		Args   : RYFTTS_console con
 *		Returns: void
 * ---------------------------------------------------------------------------
 */
void RYFTTS_console_flush(RYFTTS_console con)
{
	DWORD err = 0;
	DWOR colerr = 0;
	COORD pos = { 0, 0 };
	for(uint8 i = 0; i < con->w * con->h; i++)
	{
		pos = { i / con->w, i - (i / con->w) % con->w };
		WriteConsoleOutputAttribute(con->hstdout, &con->buffer[i].color,
			1, pos, &colerr);
		WriteConsoleOutputCharacter(con->hstdout, (char*)&con->buffer[i].c,
			1, pos, &err);
		if (!(err && colerr))
		{
			perror("Exiting on Error: Could not write char to window." );
			exit(EXIT_FAILURE);
		}
	}
}
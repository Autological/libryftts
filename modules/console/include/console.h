/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */

#ifndef LIBRYFTTS_CONSOLE_H
#define LIBRYFTTS_CONSOLE_H

/* Console Managers */
RYFTTS_API RYFTTS_console RYFTTS_console_init(int w, int h, bool color);
RYFTTS_API void RYFTTS_console_end(RYFTTS_console con);

/* Console IO */
RYFTTS_API void RYFTTS_console_putentryat(RYFTTS_console, uint8, 
										  RYFTTS_color_pair, uint8, uint8);
RYFTTS_API void RYFTTS_console_putc(RYFTTS_console, uint8, 
									RYFTTS_color_pair);
RYFTTS_API void RYFTTS_console_puts(RYFTTS_console, 
									const RYFTTS_console_color_cell*,
									uint16);
RYFTTS_API uint8 RYFTTS_console_getc(RYFTTS_console);
RYFTTS_API void RYFTTS_console_flush(RYFTTS_console con);

#endif
/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */

#ifndef LIBRYFTTS_FOV_H
#define LIBRYFTTS_FOV_H

RYFTTS_API RYFTTS_fov_map RYFTTS_fov_map_init(int32 w, int32 h, bool defaultView);
RYFTTS_API void RYFTTS_fov_map_setransparency(RYFTTS_fov_map map, int32 x, int32 y, bool isransparent);
RYFTTS_API RYFTTS_fov_view_map RYFTTS_fov_map_trace(RYFTTS_fov_map map, uint32 x, uint32 y, int32 radius);
RYFTTS_API bool RYFTTS_fov_map_in_view(RYFTTS_fov_view_map view, uint32 x, uint32 y);
RYFTTS_API void RYFTTS_fov_map_destroy(RYFTTS_fov_map map);
RYFTTS_API void RYFTTS_fov_view_destroy(RYFTTS_fov_view_map view);
RYFTTS_LOCAL void lerp(RYFTTS_fov_view_map view, RYFTTS_fov_map map, int32 x, int32 y, int32 px, int32 py, int32 radius);

#endif
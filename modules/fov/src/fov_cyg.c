/* ===========================================================================
 * libRyftts 0.1.0
 * Copyright (c) 2015 Luke Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *	   this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 * 	   documentation and / or other materials provided with the distribution.
 *
 *  3. The name of Luke Smith may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ===========================================================================
 */

/* ===========================================================================
 * NOTICE: This work is heavily influenced by, but in no way a derivative work
 * of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
 * as of September, 2015
 * ===========================================================================
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <libryftts.h>

RYFTTS_fov_map RYFTTS_fov_map_init(int32 w, int32 h, bool defaultView, RYFTTS_fov_algorithm algo)
{
	RYFTTS_fov_map map = NULL;
	if (w > 0 && h > 0)
	{
		map = (RYFTTS_fov_map)calloc(sizeof(RYFTTS_fov_map_struct), 1);
		map->width = w;
		map->height = h;
		map->algorithm = algo;
		map->blocksView = (bool *)calloc(sizeof(bool), w*h);
		for (int32 i = 0; i < w*h; i++)
		{
			map->blocksView[i] = defaultView;
		}
	}
	return map;
}

void RYFTTS_fov_map_set_transparency(RYFTTS_fov_map map, uint32 x, uint32 y, bool isransparent)
{
	if (map == NULL || x >= map->width || y >= map->height)
	{
		return;
	}
	map->blocksView[x + map->width * y] = !isransparent;
}

RYFTTS_fov_view_map RYFTTS_fov_map_trace(RYFTTS_fov_map map, uint32 x, uint32 y, int32 radius)
{
	RYFTTS_fov_view_map view = NULL;
	if (map == NULL || x >= map->width || y >= map->height)
	{
		return NULL;
	}
	int32 size = (2 * radius + 1)*(2 * radius + 1);
	view = (RYFTTS_fov_view_map)calloc(sizeof(RYFTTS_fov_view_map_struct), 1);
	view->radius = radius;
	view->width = 2 * radius + 1;
	view->height = 2 * radius + 1;
	view->x = x;
	view->y = y;
	view->in_sight = (bool *)calloc(sizeof(bool),size);
	for (int32 i = 0; i < size; i++)
	{
		view->in_sight[i] = false;
	}
	int32 px = radius;
	int32 py = 0;
	int32 mvx = 1 - px;

	while (px >= py)
	{
		lerp(view, map, x, y, px, py, radius);
		lerp(view, map, x, y, py, px, radius);
		lerp(view, map, x, y, py, -px, radius);
		lerp(view, map, x, y, px, -py, radius);
		lerp(view, map, x, y, -px, py, radius);
		lerp(view, map, x, y, -py, px, radius);
		lerp(view, map, x, y, -py, -px, radius);
		lerp(view, map, x, y, -px, -py, radius);
		if (mvx <= 0)
		{
			py++;
			mvx += 2 * py + 1;
		}
		else
		{
			px--;
			lerp(view, map, x, y, px, py, radius);
			lerp(view, map, x, y, py, px, radius);
			lerp(view, map, x, y, py, -px, radius);
			lerp(view, map, x, y, px, -py, radius);
			lerp(view, map, x, y, -px, py, radius);
			lerp(view, map, x, y, -py, px, radius);
			lerp(view, map, x, y, -py, -px, radius);
			lerp(view, map, x, y, -px, -py, radius);
			py++;
			mvx += 2 * (py - px) + 1;
		}
	}
	return view;
}

bool RYFTTS_fov_map_in_view(RYFTTS_fov_view_map view, uint32 x, uint32 y) {
	
	return ((unsigned)abs(x - view->x) <= view->radius && 
			(unsigned)abs(y - view->y) <= view->radius &&
			view->in_sight[x - view->x + view->radius + view->width * (y - view->y + view->radius)]);
}

void RYFTTS_fov_map_destroy(RYFTTS_fov_map map)
{
	free(map->blocksView);
	free(map);
}

void RYFTTS_fov_view_destroy(RYFTTS_fov_view_map view)
{
	free(view->in_sight);
	free(view);
}

void lerp(RYFTTS_fov_view_map view, RYFTTS_fov_map map, int32 x, int32 y, int32 px, int32 py, int32 radius)
{
	int32 dx = 0;
	int32 dy = 0;
	for (int32 i = 0; i < radius && !map->blocksView[(x + dx) + map->width * (y + dy)]; i++)
	{
		view->in_sight[dx + view->radius + view->width * (dy + view->radius)] = true;
		dx = (i * px) / radius;
		dy = (i * py) / radius;
	}
}

#include <libryftts.h>
#include <stdio.h>
#include <stdlib.h>

bool handle_keys(RYFTTS_console c, uint8 *px, uint8 *py);

int main()
{
	RYFTTS_console console = RYFTTS_console_init(16, 16, true);

	uint8 playerx = 9;
	uint8 playery = 9;
	RYFTTS_color_pair playercolor = RYFTTS_color_create_pair(RYFTTS_COLOR_WHITE, RYFTTS_COLOR_CYAN);
	RYFTTS_color_pair blankcolor = RYFTTS_color_create_pair(RYFTTS_COLOR_BLACK, RYFTTS_COLOR_BLACK);
	do {
		RYFTTS_console_putentryat(console, '@', playercolor, playerx, playery);
		RYFTTS_console_flush(console);
		RYFTTS_console_putentryat(console, ' ', blankcolor, playerx, playery);
	} while (handle_keys(console, &playerx, &playery)); 
	RYFTTS_console_end(console); 
	return EXIT_SUCCESS;
}

bool handle_keys(RYFTTS_console c, uint8 *px, uint8 *py) {
	uint8 ch = RYFTTS_console_getc(c);
	if(ch == 'e') return false;

	if(ch == 'w' || ch == 'W') *py = (*py) ? 0 : *py - 1;
	else if(ch == 's' || ch == 'S') *py = (*py == c->h-1) ? c->h-1 : *py + 1;
	else if(ch == 'a' || ch == 'A') *px = (*px) ? 0 : *px - 1;
	else if(ch == 'd' || ch == 'D') *py = (*px == c->w-1) ? c->w-1 : *px + 1;
	return true;
}
#include <libryftts.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>

int main()
{
	RYFTTS_console console = RYFTTS_console_init(32, 32, true);

	RYFTTS_fov_map map = RYFTTS_fov_map_init(32, 32, false);
	RYFTTS_fov_view_map fov_view = NULL;
	int x = 14;
	int y = 14;
	int dx = 0;
	int dy = 0;
	RYFTTS_color_pair cansee = RYFTTS_color_create_pair(RYFTTS_COLOR_BLACK, RYFTTS_COLOR_YELLOW);
	RYFTTS_color_pair noview = RYFTTS_color_create_pair(RYFTTS_COLOR_YELLOW, RYFTTS_COLOR_BLACK);

	do
	{
		
		map->blocksView[x + 32 * y] = true;
		fov_view = RYFTTS_fov_map_trace(map, 15, 15, 15);
		for (int i = 0; i < 32; i++)
		{
			for (int j = 0; j < 32; j++)
			{
				if (RYFTTS_fov_map_in_view(fov_view, i, j))
				{
					RYFTTS_console_putentryat(console, i, j, '.', cansee);
				}
				else
				{
					RYFTTS_console_putentryat(console, i, j, 'X', noview);
				}
			}
		}
		map->blocksView[x + 32 * y] = false;
		if (y == 14 && x < 16)
			x++;
		else if (x == 16 && y < 16)
			y++;
		else if (y == 16 && x > 14)
			x--;
		else if (x == 14 && y > 14)
			y--;


	} while (RYFTTS_console_getc(console) != 'e');
	RYFTTS_console_end(console); 
	return EXIT_SUCCESS;
}
#!/bin/bash

# ===========================================================================
# libRyftts 0.1.0
# Copyright (c) 2015 Luke Smith
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  1. Redistributions of source code must retain the above copyright notice,
#	   this list of conditions and the following disclaimer.
#
#  2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#   	   documentation and / or other materials provided with the distribution.
#
#  3. The name of Luke Smith may not be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY LUKE SMITH ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
# NO EVENT SHALL LUKE SMITH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ===========================================================================


# ===========================================================================
# NOTICE: This work is heavily influenced by, but in no way a derivative work
# of libtcod. libtcod can be found at https://bitbucket.org/libtcod/libtcod
# as of July, 2015
# ===========================================================================

set -e

platform='unknown'
unamestr=$(uname -o)
version='0.1.0'

cc="$(which cc)"
debug="false"
makeall="false"
maketarget=""
makemodules="ryftts "
showhelp="false"
quiet="false"
while [[ $# > 0 ]]; do
	key="$1"
	case $key in
		--cc) 
		cc="$(which $2)"
		shift
		;;
		--cpp)
		cpp="$(which $2)"
		shift
		;;
		-d|--debug)
		debug="true"
		;;
		-t|--target)
		maketarget+="$2 "
		shift
		;;
		-m|--module)
		if [[ "$(echo "x$2" | tr '[A-Z]' '[a-z')" == "xall" ]]; then
			makemodules=`(ls modules)`
			makeall="true"
		else
			[[ "x$makeall" == "xtrue" ]] || makemodules+="$2 "
		fi
		shift
		;;
		-q|--quiet)
		quiet="true"
		;;
		-h|--help)
		showhelp="true"
		break
		;;
		*)

		;;
	esac
	shift
done
if [[ "x$showhelp" == "xtrue" ]]; then
	echo "Usage: ./build.sh [options]"
	echo "Build the Ryftts library and any modules specified"
	echo ""
	echo "  --cc  CC			Set the C compiler"
	echo "  --cpp CPP			Set the C++ compiler"
	echo "  -t, --target <TARGET>		Set a make target."
	echo "			  	  This may be used multiple times."
	echo "  -m, --module <MODULE>		Set a module to be made"
	echo "			  	  This may be used multiple times."
	echo "  -d, --debug 			Turn on debugging for the build"
	echo "  -h, --help			Show this help and exit"
	exit 0
fi

if [ "x$unamestr" == 'xGNU/Linux' ]; then
	echo "Linux found..." && platform='linux'
fi

if [ "x$unamestr" == 'xFreeBSD' ]; then
	echo "FreeBSD found..." && platform='freebsd'
fi

if [ "x$unamestr" == 'xCygwin' ]; then
	echo "Cygwin found..." && platform='cygwin'
fi

if [ $platform == 'unknown' ]; then 
	echo "Platform not supported. Support Platforms are:"
	echo "... Linux"
	echo "... FreeBSD"
	echo "... Cygwin"
	echo "... Windows (through build.bat)"
	exit 1
fi

[ -d lib ] || mkdir -p lib
[ -d log ] || mkdir -p log
[ -d bin ] || mkdir -p bin
[ -d obj ] || mkdir -p obj
[ -d inc ] && [ -d inc/libryftts ] || mkdir -p inc/libryftts

mkdir -p tmp/usr/local/{bin,lib,include}

if [ "x$cc" == "x" ]; then
	echo "Please install $1 or choose a C compiler avaiable to you." \
	"... This script will resume with the cc compiler. "
	cc=$(which cc)
fi
echo "Using $cc for C compiler..."

if [[ "x$quiet" == "xtrue" ]]; then
	quiet="-s";
else
	quiet="";
fi

if [[ "x$debug" == "xtrue" ]]; then
	echo "Using debugging..."
	CC=$cc CXX=$cpp MODULES="$makemodules" DESTDIR="./tmp" \
	make -f makefiles/Makefile-$platform $quiet \
	CFLAGS+=-DDEBUG $maketarget
else
	CC=$cc MODULES="$makemodules" DESTDIR="./tmp" \
	make -f makefiles/Makefile-$platform $quiet $maketarget
fi

if [[ "$maketarget" == *"install"* ]]; then
	if [[ $platform != 'cygwin' ]]; then
		sudo cp -va tmp/* /
		[[ -L /usr/local/lib/libryftts.so ]] && sudo rm /usr/local/lib/libryftts.so;
		sudo ln -s /usr/local/lib/libryftts.so.$version /usr/local/lib/libryftts.so;
	else
		cp -va tmp/* /
		[[ -f /usr/local/lib/libryftts.dll ]] && rm /usr/local/lib/libryftts.dll;
		ln -s /usr/local/lib/libryftts.dll.$version /usr/local/lib/libryftts.dll;
	fi
fi